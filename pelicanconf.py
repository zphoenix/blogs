#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'zphoenix'
SITENAME = u'ZPHOENIX-Blog'
SITESUBTITLE = u'#FOSS #Python #GiveBack'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Asia/Calcutta'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Pagination
DEFAULT_PAGINATION = 10
PAGINATION_PATTERNS = (
    (1, '{base_name}/', '{base_name}/index.html'),
    (2, '{base_name}/page/{number}/', '{base_name}/page/{number}/index.html'),
)

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

OUTPUT_PATH = 'public/'

# THEME CONFIG
THEME = 'theme'
HEADER_COLOR = 'cyan'
COLOR_SCHEME_CSS = 'darkly.css'

STATIC_PATHS = ['assets']

AUTHORS_BIO = {
  "zphoenix": {
    "name": "zphoenix",
    "website": "http://zphoenix.gitlab.io",
    "location": "Bangalore",
    "bio": "#FOSS #Python #GiveBack"
  }
}
